"""Kvízek egyedi oldalának megvalósítása osztállyal"""
from kivy.app import App
from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from pycode.DAO.dao import FlashDao

Builder.load_file("View/kviz.kv")


class KvizScreen(Screen):
    """
    A rákattintott kvíz megjelenítése,
    megjelenik a kérdés majd gomb lenyomására látható lesz
    a kérdésre a válasz is, egy másik gombbal pedig tovább
    lehet menni a következő kérdésre, ha nincs több deaktiválódik a gomb
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        app = App.get_running_app()
        home = app.root.get_screen('home')
        self.cim = home.cim
        self.questions_and_answers = {}
        self.current_index = 0
        self.load_question()

    def go_to_home_screen(self):
        """Főoldalra ugrás"""
        App.get_running_app().add_home_screen()
        self.manager.current = "home"
        App.get_running_app().remove_kviz_screen()

    def load_question(self):
        """Dinamikus betöltése a kérdés-válaszoknak az adatbázisból a kvíz címe alapján"""
        dao = FlashDao()
        self.questions_and_answers = dao.get_kerdes_valasz(self.cim)
        if self.questions_and_answers:
            # csak gy kerdest ker el index alapjan
            question = list(self.questions_and_answers.keys())[self.current_index]
            self.ids.question_label.text = question
            self.ids.answer_label.text = ""
            if self.current_index == len(self.questions_and_answers) - 1:
                self.ids.next_button.disabled = True
            else:
                self.ids.next_button.disabled = False
        else:
            self.ids.question_label.text = "Nem találtunk kérdéseket"
            self.ids.next_button.disabled = True

    def show_answer(self):
        """gomb nyomására megmutatja  akérdésre a választ"""
        if self.questions_and_answers:
            question = list(self.questions_and_answers.keys())[self.current_index]
            answer = self.questions_and_answers[question]
            self.ids.answer_label.text = answer

    def next_question(self):
        """Következő kérdés megmutatása"""
        if self.questions_and_answers:
            self.current_index += 1
            self.load_question()

    def delete_kviz(self, cim):
        """A jelenleg megnyitott kvíz törlése az adatbázisból"""
        dao = FlashDao()
        dao.delete_kviz(cim)
        App.get_running_app().add_home_screen()
        self.manager.current = "home"
        App.get_running_app().remove_kviz_screen()
