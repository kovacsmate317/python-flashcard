"""
Regisztráció megvalósításának az osztálya
"""
from kivy.lang import Builder
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen
from kivy.uix.label import Label
from pycode.DAO.dao import FlashDao

Builder.load_file("C:\\prog\\python a gyakban\\projekt\\pycode\\View\\register.kv")


# azert static mert nem hasznal self. akarmit
def show_popup(title, content):
    """
    Felhasználónak visszajelzés ablakot generál, hibáról/megerősítésről
    adott cím, és szöveggel
    """
    popup = Popup(title=title, content=Label(text=content), size_hint=(None, None), size=(400, 200))
    popup.open()


class RegisterScreen(Screen):
    """
    Regisztráció megvalósítása, elleőrzi az adatokat,
    ha megfelelőek felviszi őket az adatbázisba, ha nem hibaüzenetet ad
    """
    def register(self):
        """
        Felviszi a usert az adatbázisba megfelelő adatokkal
        ellenőrzi, hogy túl rövid-e a jelszó vagy
        foglalt e már az email
        """
        email = self.ids.email_input.text
        password1 = self.ids.password_input1.text
        password2 = self.ids.password_input2.text

        if not email or not password1:
            show_popup("Hiba", "Add meg az email-ed és jelszavad")
            return
        if len(password1) <= 5:
            show_popup("Hiba", "A jelszónak legalább 6 karakternek kell lennie!")
            return
        if password2 != password1:
            show_popup("Hiba", "Nem egyezik a 2 jelszó!")
            return

        dao = FlashDao()

        if dao.register_user(email, password1):
            dao.register_user(email, password1)
            show_popup("", "Sikeres regisztráció!")
        else:
            show_popup("Hiba", "Ez az email foglalt")
            return

    def show_login(self):
        """Visszavisz a bejelentkezés oldalára"""
        self.manager.current = 'login'
