"""
Tanuló kártya applikáció ahol a felhasználó létrehozza
a saját kérdés-válaszait és kikérdezheti magától őket,
megkönnyebbítve a tanulást
"""
from kivymd.app import MDApp
from kivy.uix.screenmanager import ScreenManager
from pycode.Controller.home_controller import HomeScreen
from pycode.Controller.login_controller import LoginScreen
from pycode.Controller.register_controller import RegisterScreen
from pycode.Controller.uj_kartya_controller import UjKartyaScreen
from pycode.Controller.kviz_controller import KvizScreen


class FlashcardApp(MDApp):
    """
    Az applikáció alapjainak a megvalósítása (egységeg szín, oldalak kezelése)
    """
    def build(self):
        """
        A root widget létrehozására szolgáló függvény
        :return: a screenmanager objektum ami kezeli a képernyőket
        """
        self.theme_cls.theme_style = "Light"
        # teal,Lime,DeepPurple,Indigo
        self.theme_cls.primary_palette = "DeepPurple"
        # a balra swipe default animacio a screen manager valtozasnal
        # a konstruktorba: transition=RiseInTransition() pl. masert
        sm = ScreenManager()
        sm.add_widget(LoginScreen(name='login'))
        sm.add_widget(RegisterScreen(name='register'))
        # a namera kell hivatkozni valtasnal

        return sm

    def add_home_screen(self):
        """
        Főoldal hozzáadása az oldalakhoz, mivel tudnia kell az appnak,
        hogy kinek az adatait generálja le
        """
        self.root.add_widget(HomeScreen(name='home'))

    def add_uj_kartya_screen(self):
        """Új kártya készítésének az oldala hozzáadása a képernyőkhőz """
        self.root.add_widget(UjKartyaScreen(name='ujkartya'))

    def add_kviz_screen(self):
        """Kvízek megjelenítésének az oldala hozzáadása a képernyőkhőz """
        self.root.add_widget(KvizScreen(name="kviz"))

    # screen ujratölrésre
    def remove_kviz_screen(self):
        """frissítés miatt képernyő törlése (majd visszatétele másikban)"""
        kviz_screen = self.root.get_screen('kviz')
        self.root.remove_widget(kviz_screen)

    def remove_uj_kartya_screen(self):
        """frissítés miatt képernyő törlése (majd visszatétele másikban)"""
        uj_kartya_screen = self.root.get_screen('ujkartya')
        self.root.remove_widget(uj_kartya_screen)

    def remove_home_screen(self):
        """frissítés miatt képernyő törlése (majd visszatétele másikban)"""
        home_screen = self.root.get_screen('home')
        self.root.remove_widget(home_screen)

    def remove_loginscreen(self):
        """frissítés miatt képernyő törlése (majd visszatétele másikban)"""
        login_screen = self.root.get_screen('login')
        self.root.remove_widget(login_screen)

    def add_loginscreen(self):
        """Bejelentkezés oldala hozzáadása a képernyőkhőz """
        self.root.add_widget(LoginScreen(name="login"))

    def remove_registerscreen(self):
        """frissítés miatt képernyő törlése (majd visszatétele másikban)"""
        register_screen = self.root.get_screen('register')
        self.root.remove_widget(register_screen)

    def add_registerscren(self):
        """Regisztráció oldala hozzáadása a képernyőkhőz """
        self.root.add_widget(RegisterScreen(name="register"))


if __name__ == '__main__':
    FlashcardApp().run()
