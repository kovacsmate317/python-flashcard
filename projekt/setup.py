from setuptools import setup, find_packages

setup(
    name="Flashcardfej",
    version='0.2',
    packages=find_packages(),
    author='Kovács Máté',
    description='Tanuló kártyák',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    install_requires=[
        'Kivy==2.3.0',
        'kivymd==1.2.0',
        'bcrypt==4.1.2'
    ],
)
