"""
 külön osztály az adatbázis műveletek megvalósítására
"""
import os
import sqlite3
import bcrypt
from kivymd.uix.textfield import MDTextField


def verify_password(password, hashed_password):
    """a hashelt jelszó ellenőrzése"""
    return bcrypt.checkpw(password.encode('utf-8'), hashed_password)


def hash_password(password):
    """a jelszó string hashelése"""
    salt = bcrypt.gensalt()
    hashed_password = bcrypt.hashpw(password.encode('utf-8'), salt)
    return hashed_password


class FlashDao:
    """Adatbázis csatlakozás, műveletek"""
    def __init__(self):
        jelenlegi_mappa = os.path.dirname(os.path.abspath(__file__))
        self.db_path = os.path.join(jelenlegi_mappa, '..', '..', 'sql', 'flash.sqlite')

    def authenticate_user(self, email, password):
        """
        Ellenőrzi a bejelentkezési adatok helyességét,
        """
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()
        c.execute('''SELECT * FROM users WHERE email=? ''', (email, ))
        user = c.fetchone()
        conn.close()
        if user is not None:
            if verify_password(password, user[1]):
                return user
        return None

    def register_user(self, email, password):
        """
        Felhasználó regisztrációja az adatbázisba,
        ellenőrzi, hoy foglalt-e az email
        :return: Szabad-e az email
        """
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()
        # tuple kell neki legalabbis sqlitenal, igy lesz egy egy elemu tuple (extra vesszo)
        c.execute('''SELECT * FROM users WHERE email=? ''', (email,))
        talalt = c.fetchone()
        if talalt:
            conn.close()
            return False
        c.execute('''INSERT INTO users (email, jelszo) VALUES (?, ?)''', (email, hash_password(password)))
        conn.commit()
        conn.close()
        return True

    def add_new_card(self, email, title, questions, answers):
        """Új kártya db-be szúrása kérdés-válaszokkal"""
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()
        c.execute('''INSERT INTO kartyak (email, cim) VALUES (?,?)''', (email, title))
        conn.commit()
        c.execute('''SELECT MAX(id) FROM kartyak;''')
        # a fetchone is tuple-vel tér vissza attol, hogy 1 elem ezért kell a [0]
        card_id = c.fetchone()[0]

        for q, a in zip(questions, answers):
            if isinstance(q, MDTextField):
                kerdes = q.text.strip()
                valasz = a.text.strip()
            else:
                # a teszteles miatt kell, ott nem birk megadni textfieldeket
                kerdes = q
                valasz = a

            conn.execute('''INSERT INTO kerdes (kerdes, valasz, kartyaid) VALUES (?,?,?)''', (kerdes, valasz, card_id))
        conn.commit()
        conn.close()

    def get_kartyak(self, email):
        """
        email alapján lekérdezi a user kártyáit
        """
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()
        c.execute('''SELECT cim FROM kartyak where email = (?)''', (email,))
        card_names = []
        for row in c.fetchall():
            card_names.append(row[0])

        conn.close()
        return card_names

    def get_kerdes_valasz(self, cim):
        """
        cím alapján lekérdezi a user kártyájának a kérdés-válaszait
        """
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()
        c.execute('''SELECT id FROM kartyak where cim = (?)''', (cim,))
        result = c.fetchone()
        if result:
            kartya_id = result[0]
            c.execute('''SELECT kerdes, valasz FROM kerdes WHERE kartyaid = (?)''', (kartya_id,))
            questions_and_answers = {}
            for row in c.fetchall():
                kerdes, valasz = row
                questions_and_answers[kerdes] = valasz
            conn.close()
            return questions_and_answers
        else:
            conn.close()
            return None

    def delete_kviz(self, cim):
        """Cím alapján törlo az adatbázisból a kártyát, kérdésekkel együtt"""
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()
        c.execute('''DELETE FROM kartyak WHERE cim = (?)''', (cim,))
        # deletenel commitolni is kell
        conn.commit()
        conn.close()
