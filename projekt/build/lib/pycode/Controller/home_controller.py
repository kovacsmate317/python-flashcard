
"""
Főoldal megvalósításának az osztálya
"""
from kivy.app import App
from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.metrics import dp
from pycode.DAO.dao import FlashDao

Builder.load_file("View/home.kv")


class HomeScreen(Screen):
    """A bejelentkezés után az első képernyő, megjeleníti a felhasználó kártyáit
       amire rákattitva megnézheti a kérdés-válaszait, ezek mellett hozhat létre újat is
    """
    cim = ""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.load_cards()

    # kell a button mert pyban amugy nem tudja Gombra értelmezni plusz arg nélkül
    def show_uj_kartya_oldal(self, button):
        """Új kárty létrehozására való oldalra visz át"""
        self.manager.current = "ujkartya"

    def show_kviz_oldal(self, kvizcim, button):
        """A kártyára kattintáskor átvisz a kártya kérdéseinek az oldalára"""
        HomeScreen.cim = kvizcim
        app = App.get_running_app()
        app.add_kviz_screen()
        self.manager.current = "kviz"
        app.remove_home_screen()

    def load_cards(self):
        """ Dinamikusan megjeleníti a felhasználó kártyáit az adatbázisból"""
        self.ids.gridlayout.clear_widgets()
        app = App.get_running_app()
        login_screen = app.root.get_screen('login')
        email_log = login_screen.email_log
        dao = FlashDao()
        kartyak = dao.get_kartyak(email_log)

        ujbutton = Button(
            text="Új\nkártya",
            size_hint=(1, None),
            height=dp(100),
            background_color=[0.294, 0.2, 0.51, 1],
            halign='center'
        )
        ujbutton.bind(on_press=self.show_uj_kartya_oldal)
        self.ids.gridlayout.add_widget(ujbutton)

        for kartya in kartyak:
            button_text = kartya
            if len(button_text) > 10:
                button_text = "\n".join([button_text[i:i + 10] for i in range(0, len(button_text), 10)])
            button = Button(text=button_text, size_hint=(1, None), height=dp(100),
                            background_color=[0.6, 0.6, 0.6, 1], halign='center')
            # kell  a kartya= kartya mert amugy mindig a legutolso lenne bindolva
            button.bind(on_press=lambda instance, lambda_kartya=kartya: self.show_kviz_oldal(lambda_kartya, instance))
            self.ids.gridlayout.add_widget(button)

    def logout(self):
        """ Felhasználó kijelentkezéére, vissza dobja a Loginra"""
        self.manager.current = "login"
        App.get_running_app().remove_home_screen()
