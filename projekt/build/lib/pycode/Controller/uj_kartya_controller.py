"""Új kártya oldal osztály megvalósítása"""
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen
from kivymd.uix.textfield import MDTextField
from pycode.DAO.dao import FlashDao

Builder.load_file("View/ujkartya.kv")


def show_popup(title, content):
    """Felhasználónak visszajelzés ablakot generál, hibáról/megerősítésről"""
    popup = Popup(title=title, content=Label(text=content), size_hint=(None, None), size=(300, 200))
    popup.open()


class UjKartyaScreen(Screen):
    """Új kárty létrehozására való képernyő megvalósítása
     ahol kérdéseket és a válaszokat is megkell adni"""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.title = ""
        self.question_inputs = []
        self.answer_inputs = []

    def add_question(self, *args):
        """
        Generál 2 új input mezőt a gomb lenyomására, így több kérdés-választ lehet megadni
        """
        question_input = MDTextField(hint_text="Kérdés:", size_hint_x=0.75)
        answer_input = MDTextField(hint_text="Válasz:", size_hint_x=0.75)
        self.ids.grid.add_widget(question_input)
        self.ids.grid.add_widget(answer_input)

        self.question_inputs.append(question_input)
        self.answer_inputs.append(answer_input)

    def save_quiz(self, *args):
        """elmenti a kvíz címét, kérdéseit az adatbázisba"""
        try:
            title_input = self.ids.title_input
            title = title_input.text.strip()
            app = App.get_running_app()
            login_screen = app.root.get_screen('login')
            email_log = login_screen.email_log

            if not title:
                show_popup("Hiba", "Adj meg egy címet")
                return
            if len(title) > 25:
                show_popup("Hiba", "Max 25 hosszú cím")
                return
            dao = FlashDao()
            dao.add_new_card(email_log, title, self.question_inputs, self.answer_inputs)
            App.get_running_app().remove_home_screen()
            App.get_running_app().add_home_screen()
            self.manager.current = "home"
            App.get_running_app().remove_uj_kartya_screen()
            App.get_running_app().add_uj_kartya_screen()

        except Exception as e:
            show_popup("Hiba", f"{e}")

    def go_to_home(self):
        """Visszavisz a főoldalra"""
        self.manager.current = "home"
