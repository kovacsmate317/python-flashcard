"""bejelentkezés megvalósítása osztállyal"""
import bcrypt
from kivy.app import App
from kivy.metrics import dp
from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRaisedButton
from kivymd.uix.textfield import MDTextField
from kivymd.uix.toolbar import MDTopAppBar
from pycode.DAO.dao import FlashDao


def verify_password(password, hashed_password):
    """a hashelt jelszó ellenőrzése"""
    return bcrypt.checkpw(password.encode('utf-8'), hashed_password)


class LoginScreen(Screen):
    """Bejelentkezés oldala, egy emaillal és egy jelszóval,
    ha megfelelők az adatok átvisz a főoldalra, ha nem figyelmezteti
    a felhasználót mi a hiba"""
    email_log = ""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setup_ui(App.get_running_app())

    def setup_ui(self, app):
        """Az alap felhasználói felület legenerálása"""
        top_app_bar = MDTopAppBar(title="Bejelentkezés", elevation=2, pos_hint={"top": 1})
        self.add_widget(top_app_bar)

        box_layout = MDBoxLayout(orientation='vertical', size_hint=(None, None), size=(dp(200), dp(250)),
                                 pos_hint={'center_x': 0.5, 'center_y': 0.5}, spacing=dp(10))
        self.add_widget(box_layout)

        self.email_input = MDTextField(id='email_input', hint_text='Email', icon_right="email-outline")
        box_layout.add_widget(self.email_input)

        self.password_input = MDTextField(id='password_input', hint_text='Jelszó', password=True, icon_right="eye-off")
        box_layout.add_widget(self.password_input)

        login_button = MDRaisedButton(text='Bejelentkezés', md_bg_color=app.theme_cls.primary_color)
        login_button.bind(on_press=self.login)
        box_layout.add_widget(login_button)

        registration_button = MDRaisedButton(text='Nincs még fiókod?', md_bg_color=app.theme_cls.primary_color)
        registration_button.bind(on_press=self.show_registration)
        box_layout.add_widget(registration_button)

    def login(self, button):
        """bejelentkezés gomb lenyomására ellenőrzi az adatokat"""
        email = self.email_input.text
        password = self.password_input.text

        if not email or not password:
            self.show_popup("Hiba", "Add meg az emailed és a jelszavad")
            return

        dao = FlashDao()
        user = dao.authenticate_user(email, password)

        if user:
            LoginScreen.email_log = email
            App.get_running_app().add_home_screen()
            App.get_running_app().add_uj_kartya_screen()
            self.manager.current = 'home'
            App.get_running_app().remove_loginscreen()
            App.get_running_app().add_loginscreen()
            App.get_running_app().remove_registerscreen()
            App.get_running_app().add_registerscren()

        else:
            self.show_popup("Hiba", "Érvénytelen email vagy jelszó")

    def show_registration(self, instance):
        """Regisztráció oldalra visz át"""
        self.manager.current = 'register'

    @staticmethod
    def get_email():
        """visszatér a bejelentkezett emaillal"""
        return LoginScreen.email_log

    @staticmethod
    def show_popup(title, content):
        """Felhasználónak visszajelzés ablakot generál, hibáról/megerősítésről"""
        popup = Popup(title=title, content=Label(text=content), size_hint=(None, None), size=(300, 200))
        popup.open()
