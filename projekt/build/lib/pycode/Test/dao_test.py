"""
Adatbázis csatlakozás, műveletek teszt
"""
import unittest
from pycode.DAO.dao import FlashDao


class TestFlashDao(unittest.TestCase):
    """
    Db tesztelés unittesztekkel
    """
    def setUp(self):
        """tesztekhez való alap lépések előkészítése: dao példány műveletekhez"""
        self.dao = FlashDao()

    def test_register_user(self):
        """
        regisztráció sikerességére teszt
        !!új nevet kell adni mindig vagy fail lesz mert van már olyan email!!
        """
        self.assertTrue(self.dao.register_user("test1060@example.com", "password"))

    def test_register_user_existing(self):
        """
        regisztráció sikerességére teszt
        !!új nevet kell adni mindig vagy fail lesz mert van már olyan email!!
        """
        self.assertFalse(self.dao.register_user("mar@regisztralva.com", "password"))

    def test_authenticate_user_existing(self):
        """
        Az adatok hitelesítésére szolgáló teszt
        ha helyesek az adatok, van már ilyen fiók, jó jelszóval -> pass
        """
        user = self.dao.authenticate_user("mar@regisztralva.com", "password")
        self.assertIsNotNone(user)
        self.assertEqual(user[0], "mar@regisztralva.com")

    def test_authenticate_user_non_existing(self):
        """
        nem létező user hitelesítése
        """
        self.assertIsNone(self.dao.authenticate_user("nincs@ilyen.com", "password"))

    def test_add_new_card(self):
        """
        teszteli a kártya beszúrás, lekérdezés sikerességét
        """
        questions = ["Question 1", "Question 2"]
        answers = ["Answer 1", "Answer 2"]
        self.dao.add_new_card("test@example.com", "Test Card", questions, answers)
        card = self.dao.get_kerdes_valasz("Test Card")
        self.assertIsNotNone(card)

    def test_delete_kviz(self):
        """
        kártya törlés sikerességének a tesztje
        """
        self.dao.delete_kviz("Test Card")
        self.assertIsNone(self.dao.get_kerdes_valasz("Test Card"))


if __name__ == '__main__':
    unittest.main()
